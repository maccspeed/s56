import {
    Container, Col, Row, Navbar, Nav,
    Form, FormControl, Button,
  } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
  import './AppNavbar.css';
  
  function AppNavbar() {
    return(
    <Container fluid style={{borderBottom:"1px solid #000"}}>
      <Row>
        <Col md={4}>
          <Navbar.Brand href="#home" id="navLogo">Lorem ipsum</Navbar.Brand>
        </Col>
        <Col md={8}>
          <Row id="navForm">
            <Form className="d-flex container-fluid">
              <FormControl type="text" placeholder="Search" className="mr-sm-2" />
              <Button id="navFormButton">Search</Button>
            </Form>
          </Row>
          <Row style={{backgroundColor:"#222"}}>
            <Navbar expand="sm" id="navbar">
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto my-2">
                  <Nav.Link as={NavLink} to="/" className="text-white">Home</Nav.Link>
                  <Nav.Link href="#about" className="text-white">About</Nav.Link>
                </Nav>
                <Nav className="">
                  <Nav.Link as={NavLink} to="/signup" id="signUp">Sign Up</Nav.Link>
                </Nav>
              </Navbar.Collapse>
            </Navbar>
          </Row>
        </Col>
      </Row>
    </Container>
    )
  }
  
  export default AppNavbar;