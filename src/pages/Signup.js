import { useState, useEffect } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { /* Redirect, */ useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Signup() {
    const history = useHistory();

    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');

    function signUp(e) {
        e.preventDefault();

        fetch('http://localhost:4000/users/register', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if (data === true) {
                setEmail('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                    title: 'Registration successful',
                    icon: 'success',
                    text: 'Welcome to Zuitt!'
                });

                history.push("/");
            } else {
                Swal.fire({
                    title: 'Something wrong',
                    icon: 'error',
                    text: 'Please try again.'
                });
            }
        })
    }

    useEffect(() => {
        if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password1, password2]);

    return (
    <Container fluid className="m-5">
        <Form onSubmit={(e) => signUp(e)}>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email} 
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password1} 
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value={password2} 
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    </Container>
    );
}